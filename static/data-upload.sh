#!/usr/bin/env bash

# define TOKEN and URLBASE
source ~/data-config.sh

FILE="$1"
EXT="${FILE##*.}"

FILENAME=$(curl -f -H "X-Token: $TOKEN" --data-binary "@$FILE" $URLBASE)
if [ $? -ne 0 ]
then
    notify-send "Uploader" "Couldn't upload"
else
    echo -n "$URLBASE/$FILENAME.$EXT" | xclip -i -selection clipboard
    notify-send "Uploader" "Uploaded successfully"
fi
