#!/usr/bin/env bash

ID=$(dd if=/dev/urandom bs=1 count=5 status=none | base32)
FILE=/tmp/scrot$ID.png

scrot -z $FILE $@
if [ ! -e $FILE ]
then
    notify-send "Screenshot" "Failed"
    exit
fi
pinta $FILE
bash ~/data-upload.sh $FILE
rm $FILE
