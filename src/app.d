import rsht.application;
import rsht.config;
import rsht.httpstatus;
import rsht.mime;
import rsht.util;

import std.conv;
import std.datetime;
import std.file;
import std.json;
import std.random;
import std.regex;
import std.stdio : File, writeln;
import std.string;
import std.uri;
import std.uuid;

void tmpl(ResponseMessage* res, string title, string content) {
    res.header("Content-Type", "text/html");

    yieldchunk(format(`<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="TuxCrafting's website" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="/static/style.css" />
        <title>%s</title>
    </head>
    <body>
        <h1 class="center">TuxCrafting's website</h1>
        <ul class="center" id="menu">
            <li><a href="/">Home</a></li>
            <li>
                <a href="#">Projects</a>
                <ul>
                    <li><a href="/projects/rsht">RSHT</a></li>
                    <li><a href="/projects/pearchat">PearChat</a></li>
                    <li><a href="/projects/naas">NaaS</a></li>
                </ul>
            </li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/contact">Contact</a></li>
        </ul>
        <hr />
        %s
        <hr />
        <p class="center">
            <a href="http://www.catb.org/hacker-emblem/">
                <img src="/static/glider.png" alt="hacker emblem" />
            </a>
        </p>
    </body>
</html>`, title, content));
}

void serveFile(string file) {
    File f = File(file, "r");
    yield(f.size.to!string);
    char[1024] buf;
    while (!f.eof) {
        auto slice = f.rawRead(buf);
        yield(cast(string)slice);
    }
}

immutable string ANY = `^.*$`;
static bool PROD;
static string TOKEN;

static this() {
    PROD = exists("production");
    TOKEN = readText("token.txt");
}

void main() {
    Application app = new Application;

    app.generalLog = new FileLogger("general.log");
    app.accessLog = new FileLogger("access.log");

    JSONValue blog = readText("blog.json").parseJSON;

    app.on(ANY, ANY, Method.ALL, (req, res) {
        res.header(
            "Strict-Transport-Security",
            "max-age=2147483647; includeSubDomains"
        );
        res.header("X-Powered-By", "RSHT/" ~ VERSION);
        //if (req.destAddress.toAddrString != "127.0.0.1") {
        if (PROD && req.getHeaderValue("X-IsHTTPS") == null) {
            res.statusCode = 301;
            res.header("Content-Type", "text/html");
            req.path.protocol = "https";
            req.path.host = "tuxcrafting.cf".matchFirst(regex(ANY));
            res.header("Location", req.path.toString);
            yieldchunk(format(`<!DOCTYPE html>
<html>
    <head>
        <title>Permanent redirect</title>
    </head>
    <body>
        <h1><a href="%s">Please don't use plain HTTP, it's 2018!</a></h1>
    </body>
</html>`, req.path.toString));
        }
    }, true);

    app.on((req, res, status, msg) {
        tmpl(res, format("Error %s", status), format(
            `<h1>%d %s</h1>
<h3>%s</h3>`,
            status,
            StatusCodes[status],
            msg
        ));
    });

    app.on(ANY, `^/$`, Method.GET, (req, res) {
        tmpl(res, "Homepage", `<p>Welcome to my website!</p>
<p>
    I'm a 14 years old French-Israeli coder, and I like to 
    play around with low-level things (especially CPU 
    architectures, networking, and cryptography).<br />
    For example, this website is running on my web server, 
    <a href="https://gitlab.com/tuxcrafting/rsht">RSHT</a>!
</p>
<p class="strikethrough">
    Don't expect perfect (or even good) uptime, RSHT is 
    still in heavy development and totally broken.
</p>
<p>
    Actually, it's holding up pretty good.
</p>
<p>Also, here's my PGP key:</p>
<pre class="pgp-key">
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFrYo1YBCADf/9eYZV5oC3IYMoTHXAwPNEXdnW8fJOPbNa6IK8WvmMY5r375
ppDtM4nIgWAXCLz4KWt++ly7eaj5yMjL8xrXvmUS1pSIUmZoWTadR+9dd9msg0qM
Qx+PrW6XEbphhp4Ou4vRddFrt223mtVX5pqaFuv4UmFK8LRyX+Y/gR7OlvZKYQVS
OYzFD45U4O5OwZBjsieUXZiHQfZfOIK4lXr4SudjpsQ70DQd9PhgROrrPkLNRBZn
d6pUl+PIhpbSajfvguac4mgP575lMdPeF6mGn94TIHFd7a0qbPkQ1ZF62eGoglkF
5lh0K0tsqrSQzN0DtftHnMAgRLXhhljOk3VPABEBAAG0H0VsaWUgWWViZ3VpIDxl
bGllQGFncmlhdGVzLmNvbT6JAVQEEwEIAD4WIQTmX/AtN6pOX+ne56rVFgPWbDKu
+gUCWtijVgIbAwUJA8JnAAULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRDVFgPW
bDKu+uyCB/432MxQTKWBKwrHT0xs/lN5F8lq866XDjBqZGzJRbGsnPLPeNpxLNqc
BuboZ2hLiM/peoMhdBN/m/alDAtxlFDNg5mNttpRRLgMIobq4mWog8OFd+A/HP2E
5yEGLEJaCSdto6H4MsbJYHJsksMK1v06oKCCfjv4Px3ifuX3G9LbHyIEWgvf7ICW
gAAthMjaZUtKrH7d76bKUU6U+1Y7+vBOWWl7aIGS6xFO+5Bzyj/B2MjtEgQGKygp
VFtu/y+G7WkgfyiTqW8rcDn1fqylKsDifOLpqp2IE2a0K+lEvN0jcAIvsVhZuTfI
ZjKljicdQWzKKWiZNZ1fLd+E1UJ+SucFuQENBFrYo1YBCAC5eSAO2oI649dT+ZOs
Jbt371Ypsi4EieH8k+iMmcYoormAp11B1a7ddUHzgPfNSBOCVPeZID8jpkiVr7GQ
Hhe2T7ikFQnmkOvaDFyM4aXCwI1kNLv5ENBPZHKG4WncHqzo/q4rFjxuSAouB2Il
AuSmUfea/DfV9CJupRblvDY2n+kQ8XAHjh9X+Pc3sA1w67WeRXpfyD+q0amWyN8l
pg0e6ya0w8GIu4NW3sNyTzsubHxc4lFpy5AfWA/BINCNtVf+whUAPnDdB09oYc5b
C4L65l4VokA6woif0eu1SA/ULbU09xI4UllcXCRhxXXhJhD1EmQKAVVkxtEHfsYx
yLiLABEBAAGJATwEGAEIACYWIQTmX/AtN6pOX+ne56rVFgPWbDKu+gUCWtijVgIb
DAUJA8JnAAAKCRDVFgPWbDKu+pV+B/4k6MHTH96ZaWxOUuk8Eh0mLRnpFshx7SCo
LPG0stAgClGDKfdmuIUVdLZKwWUsbp/HdAgBpGnN1O/YCmzrr28hNwoMcdrMCqxS
9wTEMSM2IeJ26Uamvq0/+d6zYC7tD5VgdQLZAswSQv2JKj4hBjf6mpKXN1B5k+kv
pzhy238aLTdU6mVjG2vmpa67dVCyoJi0cllr57WY8L7xMiiaqPtOVMi/B3kBt5x+
tv2E/YDxK4n2/RytajsaK+aTr8oi130SsNlHE/VnTmIhicGK81V8uypGH6j7GNwq
+H/B3IXYFMmimfILmy4Xo+EPiKM6wyo/rnW0a44Y+iFIptoVq9ch
=jEYA
-----END PGP PUBLIC KEY BLOCK-----
</pre>`);
    });

    app.on(ANY, `^/projects/rsht/?$`, Method.GET, (req, res) {
        tmpl(res, "RSHT", `<p>
    RSHT (Named after רשת, the Hebrew word for network, pronounced [reʃet])
    is a simple, light and fast web server framework written in D.
</p>
<p>This page will host documentation for RSHT, eventually.</p>
<p>
    <a href="https://gitlab.com/tuxcrafting/rsht">Git repository</a>
</p>`);
    });

    /*
    app.on(ANY, `^/projects/pearchat/?$`, Method.GET, (req, res) {
        tmpl(res, "PearChat", `<p>
    PearChat is a decentralized, fully end-to-end encrypted, secure and
    privacy respecting chat platform.
</p>
<p>
    Well, kinda, it's still in development, and there's nothing more written
    than a basic MarkDown file detailing the protocol.
</p>
<p>
    <a href="https://git.tuxcrafting.cf/pearchat">Development is done here</a>
</p>`);
    });
    */

    app.on(ANY, `^/projects/naas/?$`, Method.GET, (req, res) {
        tmpl(res, "NaaS", `<p>
    /dev/null as a Service (NaaS) is an incredibely useful API, allowing you
    to easily dispose of unneeded data over the network.
</p>
<p>
    It is very easy to use, being provided as a RESTful API: simply send the
    data to <a href="/dev/null">/dev/null</a> in the body of a POST request.
</p>`);
    });

    app.on(ANY, `^/blog/?$`, Method.GET, (req, res) {
        res.maxAge = 3600;

        string posts = "";

        foreach (JSONValue x; blog.array) {
            posts = format(
                `<li><a href="/blog/%s">%s - %s</a></li>`,
                x["id"].str,
                x["timestamp"].str,
                x["title"].str,
            ) ~ posts;
        }

        tmpl(res, "Blog", format(
            `<h2>TuxCrafting's blog</h2><ul>%s</ul>`,
            posts
        ));
    });

    app.on(ANY, `^/blog/([a-z0-9\-]+)/?$`, Method.GET, (req, res) {
        JSONValue post;
        bool found = false;

        foreach (JSONValue x; blog.array) {
            if (x["id"].str == req.path.path[1]) {
                post = x;
                found = true;
                break;
            }
        }

        if (!found) {
            throw new HTTPError(404, "Unknown resource");
        }

        tmpl(res, post["title"].str, format(
            `<h2>%s</h2><h5>%s</h5>%s`,
            post["title"].str,
            post["timestamp"].str,
            post["body"].str
        ));
    });

    app.on(ANY, `^/admin/blog/post/?$`, Method.GET, (req, res) {
       tmpl(res, "Post blog post", `<form method="POST"
action="/admin/blog/post">
    <label for="title">Title</label>
    <input name="title" type="text" />
    <br />

    <label for="token">Token</label>
    <input name="token" type="password" />
    <br />

    <textarea name="body" rows="20" cols="100"></textarea>
    <br />

    <input type="submit" value="Let's go" />
</form>`);
    });

    app.on(ANY, `^/admin/blog/post/?$`, Method.POST, (req, res) {
        res.maxAge = 0;
        Path p;
        p.parse("a://b.c/d?" ~ req.requestBody, regex(""), regex(""));
        if (p.query["token"] != TOKEN) {
            throw new HTTPError(403, "Forbidden");
        }
        UUID id = randomUUID();
        JSONValue post = [
            "id": id.toString,
            "title": p.query["title"].tr("+", " ").decodeComponent,
            "body": p.query["body"].tr("+", " ").decodeComponent,
            "timestamp": date1123(Clock.currTime),
        ];
        blog.array ~= [post];
        res.statusCode = 303;
        if (PROD) {
            res.header("Location", "https://tuxcrafting.cf/blog/"
                                   ~ id.toString);
        } else {
            res.header("Location", "http://localhost:8000/blog/"
                                   ~ id.toString);
        }
        yield("0");
    });

    app.on(ANY, `^/contact/?$`, Method.GET, (req, res) {
        tmpl(res, "Contact", `<p>Here are some ways to contact me:</p>
<ul>
    <li>Email: <a href="mailto:elie@agriates.com">elie@agriates.com</a></li>
    <li>Fediverse: <a href="https://animeisgay.com/@tuxcrafting">
        @tuxcrafting@animeisgay.com
    </a></li>
    <li>XMPP: tuxcrafting@tuxcrafting.cf</li>
</ul>`);
    });

    app.on(ANY, `^/static/([^/]+)(\.[^/]+)$`, Method.GET, (req, res) {
        string file = "static/" ~ req.path.path[1] ~ req.path.path[2];
        if (exists(file)) {
            res.header("Content-Type", getMIME(req.path.path[2]));
            serveFile(file);
        } else {
            throw new HTTPError(404, "Unknown resource");
        }
    });

    app.on(ANY, `^/robots.txt$`, Method.GET, (req, res) {
        res.header("Content-Type", "text/plain");
        yieldchunk(`User-Agent: *
Disallow: /static
Disallow: /data
Disallow: /admin

User-Agent: T-800
Disallow: /sarahconnor`);
    });

    app.on(ANY, `^/ip/?$`, Method.GET, (req, res) {
        res.header("Content-Type", "text/plain");
        yieldchunk(req.address.toAddrString);
    });

    app.on(ANY, `^/data/?$`, Method.POST, (req, res) {
        string tok = req.getHeaderValue("X-Token");
        if (!tok || tok != TOKEN) {
            throw new HTTPError(403, "Invalid token");
        }

        string fname = "";
        for (long i = 0; i < 10; i++) {
            fname ~= uniform!("[]", char, char)('A', 'Z');
        }
        std.file.write("data/" ~ fname, req.requestBody);
        yieldchunk(fname);
    });

    app.on(ANY, `^/data/([^/]+)(\.[^/]+)$`, Method.GET, (req, res) {
        string file = "data/" ~ req.path.path[1];
        if (exists(file)) {
            res.header("Content-Type", getMIME(req.path.path[2]));
            serveFile(file);
        } else {
            throw new HTTPError(404, "Unknown resource");
        }
    });

    app.on(ANY, `^/dev/null/?$`, Method.GET | Method.POST, (req, res) {
        yield("0");
    });

    app.on(ANY, `^/favicon.ico$`, Method.GET, (req, res) {
        res.header("Content-Type", "image/x-icon");
        serveFile("favicon.ico");
    });

    if (PROD) {
        //app.listen(new InternetAddress("80.211.183.179", 80));
        app.listen(new InternetAddress("127.0.1.1", 8000), 255);
    } else {
        app.listen(new InternetAddress("127.0.0.1", 8000));
    }
    app.start;

    write("blog.json", blog.toJSON);
}
